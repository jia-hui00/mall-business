module.exports = {
    devServer: {
        open: true,
        port: 7000
    },
    publicPath: process.env.NODE_ENV === 'production' ? 'mall-business' : '/'
}