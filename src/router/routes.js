import Layout from '@/views/Layout'

// 只要登录就能访问的路由
export default [
    {
        name: 'Login',
        path: '/login',
        component: () => import('@/views/Login/index'),
        meta: { title: '登录页' }
    },
    {
        path: "/",
        component: Layout,
        redirect: '/census',
        meta: {
            title: '首页',
            icon: 'el-icon-s-home',
            auth: ['customer', 'admin'] // 权限允许的角色
        },
        children: [
            {
                name: 'Census',
                path: 'census',
                component: () => import("@/views/Census"),
                meta: {
                    title: '统计',
                    icon: 'el-icon-s-marketing',
                    auth: ['customer', 'admin']
                }
            }
        ]
    },
    {
        path: '/user',
        component: Layout,
        meta: { auth: ['customer', 'admin'] },
        children: [
            {
                name: 'UserCenter',
                path: '',
                component: () => import('@/views/User'),
                meta: {
                    title: '个人中心',
                    auth: ['customer', 'admin']
                }
            }
        ]
    },
    {
        name: 'Goods',
        path: '/goods',
        component: Layout,
        meta: {
            title: '商品',
            icon: 'el-icon-s-goods',
            auth: ['customer', 'admin']
        },
        children: [
            {
                name: "GoodsList",
                path: "list",
                component: () => import("@/views/GoodsList"),
                meta: {
                    title: '商品列表',
                    icon: 'el-icon-s-shop',
                    auth: ['customer', 'admin']
                }
            },
            {
                name: "GoodsEdit",
                path: "edit/:goods_id",
                component: () => import("@/views/GoodsEdit"),
                meta: {
                    title: '商品编辑',
                    auth: ['customer', 'admin']
                }
            },
            {
                name: "GoodsAdd",
                path: "add",
                component: () => import("@/views/GoodsAdd"),
                meta: {
                    title: '商品添加',
                    icon: 'el-icon-s-finance',
                    auth: ['customer', 'admin']
                }
            }
        ]
    },
    {
        path: '/category',
        component: Layout,
        meta: { title: '分类', icon: 'el-icon-printer', auth: ['admin'] },
        children: [
            {
                name: 'Category',
                path: 'manage',
                component: () => import('@/views/Category'),
                meta: {
                    title: '分类管理',
                    icon: 'el-icon-box',
                    auth: ['admin']
                }
            }
        ]
    },
]
