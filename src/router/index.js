import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";
import store from '@/store'

Vue.use(VueRouter);

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});


router.beforeEach((to, from, next) => {
    // 如果要跳转到登录页，直接跳转
    if (to.name === 'Login') {
        next()
    } else if (store.state.user.user_info) {
        // 如果当前角色有权限，就跳转
        // if (to.meta.auth.includes(store.state.user.user_info.role)) {
        //     next()
        // } else {
        //     // 否则跳转到首页
        //     next('/')
        // }
        next()
    } else {
        next({ name: 'Login', query: { target_url: to.fullPath } })
    }
})

export default router;
