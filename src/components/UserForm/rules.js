export default {
    email: [
        {
            required: true,
            message: "请填写邮箱",
            trigger: "blur"
        },
        {
            type: "email",
            message: "请填写正确的邮箱",
            trigger: "blur"
        }
    ],
    password: [
        {
            required: true,
            message: "请填写密码",
            trigger: "blur"
        },
        {
            min: 6,
            max: 16,
            message: "密码长度在6~16之间",
            trigger: "blur"
        }
    ],
    confirm_pwd: [
        {
            required: true,
            message: "请再次输入密码",
            trigger: "blur"
        }
    ],
    newPassword: [
        {
            min: 6,
            max: 16,
            message: "密码长度在6~16之间",
            trigger: "blur"
        }
    ],
    code: [
        {
            required: true,
            message: "请填写验证码",
            trigger: "blur"
        }
    ],
    username: [
        {
            required: true,
            message: "请填写用户名",
            trigger: "blur" // 默认是 input
        },
        {
            min: 3,
            max: 10,
            message: "用户名长度在3~10之间",
            trigger: "blur"
        }
    ]
}