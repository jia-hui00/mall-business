export default {
    form_rules: {
        title: [{
            required: true,
            message: '请填写商品名称',
            trigger: 'blur'
        }],
        category: [{
            required: true,
            message: '请选择商品分类',
            trigger: 'blur'
        }],
        tags: [{
            required: true,
            message: '请填写商品标签',
            trigger: 'blur'
        }],
        price: [{
            required: true,
            message: '请填写商品价格',
            trigger: 'blur'
        }],
        unit: [{
            required: true,
            message: '请填写商品单位',
            trigger: 'blur'
        }],
        status: [{
            required: true,
            message: '请设置商品状态',
            trigger: 'blur'
        }],
        images: [{
            required: true,
            message: '请填写图片地址',
            trigger: 'blur'
        }],
        inventory: [{
            required: true,
            message: '请填写商品库存',
            trigger: 'blur'
        }]
    }
}