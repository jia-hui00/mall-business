export function routes_filter(role, routes) {
    return  routes.filter(item => {
        if (item.children) {
            item.children = routes_filter(role, item.children)
        }
        if (item.meta.auth?.includes(role)) {
            return true
        }
        return false
    })
}
