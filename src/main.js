import Vue from "vue";
import VCharts from 'v-charts';
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./styles/var.less";
import "element-ui/lib/theme-chalk/index.css";
import ElementUi from "element-ui";
import { truncate, format_time } from '@/filter'

Vue.config.productionTip = false;
Vue.use(ElementUi);
Vue.use(VCharts);

Vue.filter('truncate', truncate)
Vue.filter('format_time', format_time)

// 创建实例之前，获取登录信息
store.dispatch('user/user_auth')

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");
