export default {
    namespaced: true,
    state: {
        is_collapse: false, // 侧边栏是否折叠
    },
    mutations: {
        toggle_collapse(state) {
            state.is_collapse = !state.is_collapse;
        },
    },
};
