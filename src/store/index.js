import Vue from "vue";
import { install, Store } from "vuex";
import nav from "./nav";
import user from './user'

install(Vue)

export default new Store({
    strict: true,
    modules: {
        nav,
        user
    },
});
