import request from "./request";

// 查询分类
export async function search_category(params) {
    return await request.get('/category/all', { params })
}

// 新增分类
export async function add_category(params) {
    return await request.post('/category/add', params)
}

// 编辑分类
export async function edit_category(params) {
    return await request.put('/category/edit', params)
}

// 删除分类
export async function del_category(id) {
    return await request.delete('/category/' + id)
}


