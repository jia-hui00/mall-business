import request from "./request";

// 查询产品列表
export async function search_products(params) {
    return await request.get('/products/all', { params })
}

// 新增产品
export async function add_product(params) {
    return await request.post('/products/add', params)
}

// 上传图片
export async function upload_img(params) {
    return await request.post('/upload/images', params)
}

// 编辑产品
export async function edit_product(params) {
    return await request.put('/products/edit', params)
}

// 获取产品详情
export async function get_product(id) {
    return await request.get('/products/' + id)
}

// 删除商品
export async function del_product(id) {
    return await request.delete('/products/' + id)
}


