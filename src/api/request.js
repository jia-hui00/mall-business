import axios from 'axios'
import store from '@/store'


const request = axios.create({
    baseURL: 'https://mallapi.duyiedu.com/'
})   // 创建 axios 实例

// 请求拦截
request.interceptors.request.use(config => {
    // 除了用户信息外，都需要 appkey
    if (!config.url.includes('/passport/')) {
        config.params = {
            ...config.params,
            appkey: store.state.user.user_info.appkey
        }
    }
    return config
}, error => Promise.reject(error))

// 响应拦截
request.interceptors.response.use(resp => {
    // 如果失败抛出错误信息
    const data = resp.data
    if (data.status === 'fail') {
        return Promise.reject(data.msg)
    }
    // 成功则返回数据
    return data.data
}, error => Promise.reject(error))

export default request;

