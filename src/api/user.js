import request from "./request";

// 登录
export async function login(params) {
    return await request.post('/passport/login', params)
}

// 注册
export async function logon(params) {
    return await request.post('/passport/logon', params)
}

// 找回密码
export async function find_pwd(params) {
    return await request.post('/passport/findBack', params)
}

// 修改用户信息
export async function change_user_info(params) {
    return await request.put('/passport/changeUserInfo', params)
}

// 获取验证码
export async function get_code(params) {
    return await request.post('/passport/getCode', params)
}
