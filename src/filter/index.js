/**
 * 字符串截断
 * @param {String} value 截断值
 * @param {Number} len 截断长度
 * @returns 截断后的值
 */

export function truncate(value, len = 50) {
    let result = value
    if (value.length > len) {
        result = value.slice(0, len) + ' ···'
    }
    return result
}


export function format_time(value, type = 'date') {
    const time = new Date(value),
        year = time.getFullYear(),
        month = (time.getMonth() + 1 + '').padStart(2, '0'),
        day = (time.getDate() + '').padStart(2, '0')
    if (type === 'date') {
        return `${year}年${month}月${day}`
    } else if (type === 'time') {
        const hour = (time.getHours() + '').padStart(2, '0'),
            minute = (time.getMinutes() + '').padStart(2, '0'),
            second = (time.getSeconds() + '').padStart(2, '0')
        return `${year}年${month}月${day}日 - ${hour}:${minute}:${second}`
    }
}
